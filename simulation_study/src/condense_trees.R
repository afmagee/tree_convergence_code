DS <- list.files("simulation_study/data/JC_gamma/",full.names=TRUE)

for (ds in DS) {
  
  cat(ds,"\n")
  
  # DS are stored in 2 column format, probabilities and trees
  ds.table <- read.table(ds,header=FALSE,stringsAsFactors=FALSE,nrows=4096)
  
  # truncate
  probs <- ds.table$V1
  prob_cutoff <- min(which(cumsum(probs) >= 0.95)) # if there are none, this return Inf and the cutoff will be 4096 trees
  cutoff <- ifelse(prob_cutoff > 4096, 4096, prob_cutoff)
  ds.table <- ds.table[1:cutoff,]

  # write
  out.file <- gsub("/JC_gamma/","/",ds)
  out.file <- gsub("_uniq_shapes_C_sorted_by_PP","_best_trees",out.file)
  write.table(ds.table,file=out.file,quote=FALSE,row.names=FALSE,col.names=FALSE)
}
